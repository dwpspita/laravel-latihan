<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DaftarController extends Controller
{
    public function form(){
        return view('page.daftar');
    }

    public function submit(Request $request){
        $first_name = $request ['first_name'];
        $last_name = $request['last_name'];
        return view('page.welcome' , compact('first_name','last_name'));

    }
}
