@extends('layout.utama')

@section('judul')
    Halaman Utama
@endsection

@section('isi')
    
<h1>SanberBook</h1>
<h2>Social Media Santai Berkualitas</h2>
<p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>
<h3>Benefit Join di SanberBook</h3>
<ul>
    <li>Mendapatkan motivasi dari sesama Developer</li>
    <li>Shari knowledge dari para mastah Sanber</li>
    <li>Dibuat oleh calon Web Developer Terbaik</li>
</ul>
<h3>Cara Bergabung ke SanberBook</h3>
<ol>
    <li>Mengunjungi Website ini</li>
    <li>Mendaftar di <a href="/pendaftaran">Form Sign Up</a></li>
    <li>Selesai</li>
</ol>

@endsection