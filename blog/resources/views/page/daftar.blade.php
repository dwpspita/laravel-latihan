@extends('layout.utama')

@section('judul')
    Halaman Utama
@endsection

@section('isi')
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/menu" method="POST">
        @csrf
        <label for="">First Name</label> <br><br>
        <input type="text" name="first_name"> <br><br>
        <label for="">Last Name</label> <br><br>
        <input type="text" name="last_name"> <br><br>
        <label for="">Gender</label> <br><br>
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female <br>
        <input type="radio" name="gender">Other <br><br>
        <label for="">Nationality</label> <br><br>
        <select name="nationality">
            <option value="1">Indonesian</option><br>
            <option value="2">Malaysian</option><br>
            <option value="3">American</option>
        </select> <br><br>
        <label >Language Spoken</label><br><br>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Other <br><br>
        <label>Bio</label> <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">
    </form>
@endsection