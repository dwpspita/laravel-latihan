@extends('layout.utama')

@section('judul')
    Menu
@endsection

@section('isi')
    <h1>Selamat Datang {{$first_name}} {{$last_name}} !!</h1>
    <h2>Terima Kasih Telah Bergabung di SanberBook. Social Media Kita Bersama!</h2>
@endsection
